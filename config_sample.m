function [my_root, my_data_folder] = my_config()
    %% Copy this config file, fill in the paths, and name it "my_config.m"
    my_root = './'; % Path to roskilde-eeg-pipeline folder
    my_data_folder = ''; %path to data
end